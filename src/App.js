import React from 'react';
import './App.css';
import Searcher from './pages/Searcher';
import Quiz from './pages/quiz/Quiz';
import {HeroProvider} from './HeroProvider';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import './animate.css';

function App() {

  return (
    <div className="App">
      <HeroProvider>
        <Router>
          <Switch>
            <Route exact path="/" component={Searcher} />
            <Route exact path="/Quiz" component={Quiz} />
          </Switch>
        </Router>
      </HeroProvider>
      <div className="footer">
        <h2>Made by Facundo Ferreira La Valle</h2>
      </div>
    </div>
  );
}

export default App;
