const baseApi = "https://www.superheroapi.com/api.php/";
const access = "10219666660726588";

const attentionSpan = 7000;

export const getAllHeros = (callBack)=> {

    const finalUrl = baseApi+access+'/search/spider';
    fetch(finalUrl)
    .then(data => {
        return data.json()
    })
    .then(data => {
        callBack(data ? data.results : "")
    })
    .catch(err => console.log(err));
}

export const getHero = (name, callBack) => {
    const finalUrl = baseApi+access+'/search/'+name;
    fetch(finalUrl)
    .then(data => {
        return data.json()
    })
    .then(data => {
        callBack(data ? data.results : "")
    })
    .catch(err => console.log(err));
}

export const getHeroById = (id, callBack, errorCall) => {
    const finalUrl = baseApi+access+'/'+id;
    var callOver = false;
    setTimeout(()=>{
        if (!callOver && errorCall){
            callOver = true;
            errorCall("Timeout!");
        }
    }, attentionSpan)
    fetch(finalUrl)
    .then(data => {
        return data.json()
    })
    .then(data => {
        if (!callOver){
            callOver = true;
            callBack(data ? data : "")
        }
    })
    .catch(err => {
        if (!callOver){
            callOver = true;
            errorCall(err ? err : "")
        }
    });
}