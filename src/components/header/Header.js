import React, { useState, useContext } from 'react';
import {getHero} from '../../HeroApi.js';
import {Link} from 'react-router-dom';
import {HeroContext} from '../../HeroProvider';
import './Header.css';

const doneTypingInterval = 1000;
let typingTimer;

export const Header = ({showSearch}) => {
    const {setHeros, fetching, setFetching} = useContext(HeroContext);
    const [searchValue, setSearch] = useState('');
    const [showDropDown, setShowDropDown] = useState(false);
    
    const searchHero = (heroName) => {
      getHero(heroName, (heroes)=>{
        setHeros(heroes);
        setFetching(false)
      });
    }
    const handleChange = (value) => {
      value = value ? value : ''
      setFetching(true)
      setSearch(value)
      clearTimeout(typingTimer);
      if (value === ''){
        typingTimer = setTimeout(()=>searchHero('spider'), doneTypingInterval)
      }else{
        typingTimer = setTimeout(()=>searchHero(value), doneTypingInterval)
      }
    }

    const renderLoading = (isLoading) => {
      return (
        isLoading ? <div className="search-loading"/> :<i class="fas fa-search"></i>
      )
    }

    const renderSearch = (showSearch = true) => {
      return showSearch ? 
        (<React.Fragment>
          {renderLoading(fetching)}  
          <input
            value={searchValue}
            onChange={e => {handleChange(e.target.value)}}  
            type="text" 
            class="form-control" 
            placeholder="Enter hero name"/>
        </React.Fragment>): null
      
    }

    const renderDropDown = () => {
      const dropDownHide = showDropDown ? '' :'hidden';
      return (
        <div className={"header-dropdown "+dropDownHide}>
          {renderDropDownItem('HOME','fas fa-home','/')}
          {renderDropDownItem('QUIZ!','fas fa-exclamation-circle','/Quiz')}
        </div>
      )
    }

    const renderDropDownItem = (text, icon, goTo) => {
      return (
        <div className="header-dropdown-item">
          <Link to={goTo}><i class={icon}></i>{text}</Link>
        </div>
      )
    }

    return (
        <div className="header">
          <div className="logo"/>
          <div className="searcher">
            <div className="form-group search-group">
              {renderSearch(showSearch)}
            </div>
            <i class="fas fa-bars clickable" onClick={()=>setShowDropDown(!showDropDown)}></i>
          </div>
          {renderDropDown()}
        </div>
      );
}
