import React, { useState, useEffect } from 'react';
import {neutral, good, bad} from '../../utils/colors';
import './Card.css';

export const Card = ({hero, height=450, flipped = false, showStats=true}) => {
    const [isFlipped, setFlipped] = useState(false);
    const [didAnimation, setDidAnimation] = useState(false);
    
    const flipCard = () =>{
        setFlipped(!isFlipped)
        
    }
    
    useEffect(()=>{
        setDidAnimation(false);
        setFlipped(flipped);
        setTimeout(()=> setDidAnimation(true), 750);
    },
    [hero, flipped])

    const flippedClass = isFlipped ? 'flipped' : '';
    const animClass = !didAnimation && !flipped ? 'animation-card-appear' : '';
    const renderFront = () =>{
        const portraitStyle = {
            backgroundImage: `url(${hero.image.url})`,
        }
        return (
            <div className={"card-hero-anim-container "+animClass}>
                <div className={"card-hero-portrait"} style={portraitStyle}>
                    {!isFlipped ? <div className="card-hero-flip"></div>: null}
                    <div className="card-hero-title">{hero.name}</div>
                </div>
            </div>
            
            
        );
    }

    const renderback = ()=>{
        const alignment = hero.biography.alignment;
        const aligmnentColor = 
            (alignment==='good') ? good : 
            (alignment==='bad') ? bad: neutral;
        const portraitStyle = {
            background: `linear-gradient(${aligmnentColor},${aligmnentColor}), url(${hero.image.url}) center no-repeat`
        }
        const stats = hero.powerstats;
        const name = hero.biography['full-name'] ? hero.biography['full-name'] : '???'
        return (
            <div className={"card-hero-portrait back "} style={portraitStyle}>
                <div className="card-hero-publisher">{hero.biography.publisher && hero.biography.publisher!=='null' ? hero.biography.publisher : '???'}</div>
                <div className="card-hero-stats">
                    {renderStat('https://i.imgur.com/aDyN0Uv.png','Combat',stats.combat)}
                    {renderStat('https://i.imgur.com/4GsWTtH.png','Power',stats.power)}
                    {renderStat('https://i.imgur.com/IFBX7JE.png','Durability',stats.durability)}
                    {renderStat('https://i.imgur.com/SXOkeJo.png','Strength',stats.strength)}
                    {renderStat('https://i.imgur.com/NYG1sot.png','Intelligence',stats.intelligence)}
                    {renderStat('https://i.imgur.com/GuhgDlR.png','Speed',stats.speed)}
                </div>
                <div className="card-hero-details">
                    <span className="card-hero-details-title">First Appearance</span>
                    <p>{hero.biography['first-appearance']}</p>
                    <span className="card-hero-details-title">Occupation</span>
                    <p>{hero.work.occupation}</p>
                    
                </div>
                <div className="card-hero-title">{name}</div>
            </div>
        )
    }

    const renderStat = (statIcon, name, value)=> {
        const iconStyle = {
            backgroundImage: `url(${statIcon})`,
        }
        if (value==='null') value='???'
        if (!showStats) return null;
        return (
            <div className="stat">
                <div className="stat-title">
                    <div  className="stat-icon" style={iconStyle}/>
                    <span className="stat-name">{name}</span>
                </div>
                <span className="stat-value">{value}</span>
            </div>
        )
    }

    const customStyle = {
        "height": `${height}px`
    }

    return (
        <div 
            className={"card-hero "+flippedClass}
            onClick={()=>flipCard()}
            style={customStyle}>
            <div className="card-hero-inner">
                {renderFront()}
                {renderback()}
            </div>
        </div>
    );
    
}