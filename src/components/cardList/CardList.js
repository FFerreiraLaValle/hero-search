import React, { useContext } from 'react';
import {HeroContext} from '../../HeroProvider';

import {Card} from './Card';
import './Card.css';

export const CardList = () => {
    const {heros} = useContext(HeroContext)
    const heroes = (heros) ? 
        heros.map(e => (<Card hero={e} triggerAnim={true}/>)) 
        : <h2>There are no super heroes here... Bummer!</h2>; 
    return (
        <div className="card-list">
            {heroes}
        </div>
    )
}