import React, { useEffect, useContext, useState } from 'react';
import {neutral, good, bad} from '../../utils/colors';
import Confetti from 'react-confetti'
import {Card} from '../cardList/Card';

export const HeroToGuess = ({hero, fetching = false, party=false, onLoadFailed, gameEnded=false}) => {

    const [loading, setLoading] = useState(fetching);
    const [showHero, setShowHero] = useState(false);

    useEffect(()=>{
        setLoading(fetching);
        if (gameEnded){
            setTimeout(()=>setShowHero(true), 3000);
        }else{
            setShowHero(false);
        }
    },[fetching, gameEnded]);

    const alignment = (hero.biography) ? hero.biography.alignment : '';
    const aligmnentColor =
            (loading) ? 'transparent':  
            (alignment==='good') ? good : 
            (alignment==='bad') ? bad : neutral;
    const style = {
        borderColor: `${aligmnentColor}`
    }
    
    const src = (hero && hero.image && !loading) ? `${hero.image.url}`:'https://i.imgur.com/OLrLOP9.gif';

    return (
        <div className="hero-to-guess">
            <Confetti
                height={900} 
                initialVelocityY={4} 
                gravity={0.1} recycle={false} 
                numberOfPieces={party ? 500 : 0}
                onConfettiComplete={confetti => {
                    confetti.reset()
                  }}
                />
            {gameEnded ? <Card hero={hero} height={320} flipped={showHero} showStats={false}/> : (<img className="hero-picture" style={style} src={src} onError={()=>fetching ? null : onLoadFailed()}/>)}
        </div>
    )
}