import React, { useState, useEffect } from 'react';
import { Paper } from '@material-ui/core';
import './AnswerManager.css';


export const AnswerManager = ({answer, otherOptions, onWin, onLose, finalLength, fetching=false}) => {
    
    const [options, setOptions] = useState([...otherOptions]);
    const [showResult, setShowResult] = useState(false);
    const [pickedOption, setPickedOption] = useState(-1);

    useEffect(()=>{
        setShowResult(false);
        setPickedOption(-1);
        const tempOptions = [...otherOptions];
        const correctIndex = Math.floor(Math.random() * otherOptions.length);
        tempOptions.splice(correctIndex,0,answer);
        setOptions(tempOptions);
    },[answer,otherOptions, fetching]);

    const renderOptions = () =>{
        const finalOptions = [];
        
        if (fetching){
            let index = 0;
            while (finalLength && index < finalLength){
                finalOptions.push(renderOption({name: '...'}, index))
                index++;
            }
        }else{
            finalOptions.push(...options.map((opt,index) => renderOption(opt,index)));
        }

        return finalOptions;
    }
    
    const renderOption = (opt, index) => {
        const isCorrectAnswer = (opt.id === answer.id);
        let btnClass = (index+1)%4===0 ? 'btn-warning' :
                        (index+1)%3===0 ? 'btn-info' :
                        (index+1)%2===0 ? 'btn-success':
                        'btn-primary'
        if (showResult){
            btnClass = isCorrectAnswer ? 'btn-success animated tada' : (pickedOption===opt.id) ? 'animated shake btn-danger':'btn-secondary';
        }
        return (
            <button key={'answear-'+index+'-opt.name'} className={"hero-option btn "+btnClass} onClick={()=>checkForWin(opt)}>
                {showResult ? (opt.id === answer.id) ? <i class="opt-result fa fa-check-circle" aria-hidden="true"/> : <i class="opt-result fa fa-times-circle"/> : ''} {opt.name}
            </button>
        )
    }

    const checkForWin = (opt) => {
        setPickedOption(opt.id);
        if (!showResult && !fetching){
            if (opt.id === answer.id){
                if (onWin) onWin();
    
            }else{
                if (onLose) onLose(); 
            }
            setShowResult(true)
        }
        
    }
    return (
        <div className="options">
             <div className="options-list">
                {renderOptions()}
             </div>
        </div>
    )
}