import React, { useEffect, useContext, useState } from 'react';
import {getHeroById} from '../../HeroApi.js';
import {HeroContext} from '../../HeroProvider.js';
import {HeroToGuess} from '../../components/heroToGuess/HeroToGuess';
import {AnswerManager} from '../../components/answerManager/AnswerManager';
import { Header } from '../../components/header/Header.js';
import './Quiz.css'

const totalHeroes = 731;
const otherAnswearsAmount = 3;
const permaBlackList = [51,54,143,133,134]

function Quiz() {
    const [correctAnswer, setCorrectAnswer] = useState({});
    const [otherAnswers, setOtherAnswers] = useState([]);
    const [fetching, setFetching] = useState(false);
    const [score, setScore] = useState(0);
    const [attemps, setAttemps] = useState(0);
    const [party, setParty] = useState(false);
    const [gameEnded, setGameEnded] = useState(false);
    
    let tempAnswears = [];

    useEffect(() => {
        loadGame();
    }, []);

    const getOtherAnswers = (blackList) => {
        for (let i=0;i<otherAnswearsAmount;i++){
            let otherAnswearId = Math.floor(Math.random() * totalHeroes + 1);
            getHeroById(otherAnswearId, 
                (answer) => { verifyFetchedAnswer(answer, blackList)},
                (err) => handleError(blackList));
        }
    }

    const handleError = (blackList) => {
        let otherAnswearId = Math.floor(Math.random() * totalHeroes + 1);
        getHeroById(otherAnswearId, (answer) => verifyFetchedAnswer(answer, blackList), (err) => handleError(blackList));
    }

    const verifyFetchedAnswer = (answer, blackList) => {
        console.log(answer)
        const existsInBlackList = blackList.some(heroId => (answer.id === heroId));
        if (existsInBlackList){
            let otherAnswearId = Math.floor(Math.random() * totalHeroes + 1);
            getHeroById(otherAnswearId, (answer) => verifyFetchedAnswer(answer, blackList), (err) => handleError(blackList));
        }
        else{
            tempAnswears.push(answer);
            blackList.push(answer.id);
            setOtherAnswers(tempAnswears)
            if (blackList.length === otherAnswearsAmount + 1 + permaBlackList.length){
                setFetching(false);
            }
        }
    }

    const handleResult = (didPlayerWon) => {
        setAttemps(attemps+1);
        if (didPlayerWon){
            setScore(score+1);
            console.log("WIN!");
            setParty(true);
        }
        loadGame(true);
    }

    const loadGame = (secondTime) => {
        let timeToNext = 10 * 1000;
        tempAnswears = []
        if (secondTime) setGameEnded(true)
        setTimeout(()=> {
            let randomId = Math.floor(Math.random() * totalHeroes + 1)
            let blackList = [...permaBlackList];
            while (blackList.includes(randomId))
                randomId = Math.floor(Math.random() * totalHeroes + 1)
            blackList.push(randomId);
            setGameEnded(false)
            setFetching(true);
            setParty(false);
            getHeroById(randomId, setCorrectAnswer);
            getOtherAnswers(blackList);
        },(secondTime) ? timeToNext : 0)
    }

    const onAnswerImgLoadFailed = ()=> {
        console.log("Error loading img, loading next one");
        loadGame();
    }

    const content = <React.Fragment>
                        <HeroToGuess hero={correctAnswer} fetching={fetching} party={party} onLoadFailed={onAnswerImgLoadFailed} gameEnded={gameEnded}/>
                        {fetching ? 
                            <div>Loading...</div> :
                        <p className="game-score">Score: {`${score}/${attemps} (`}{Math.floor(score/(attemps==0? 1 : attemps)*100)}%)</p>
                        }
                        <AnswerManager
                                    answer={correctAnswer}
                                    finalLength={otherAnswearsAmount+1} 
                                    otherOptions={otherAnswers} 
                                    onWin={()=>handleResult(true)} 
                                    onLose={()=>handleResult(false)}
                                    fetching={fetching}/>
                        
                        </React.Fragment>

    return (
        <div className="App-quiz">
            <Header showSearch={false}/>
            <div className="app-quiz-content">
                <h2>Guess the Hero (or Villan)</h2>
                {content}
            </div>
        </div>
    );
}

export default Quiz;