import React, { useEffect, useContext } from 'react';
import '../App.css';
import {Header} from '../components/header/Header.js';
import {CardList} from '../components/cardList/CardList.js';
import {getAllHeros} from '../HeroApi.js';
import {HeroContext} from '../HeroProvider.js';

function Searcher() {
    const {setHeros} = useContext(HeroContext);

    useEffect(() => {
        getAllHeros(setHeros);
    }, []);

    return (
        <div className="App-searcher">
            <Header />
            <CardList/>
        </div>
    );
}

export default Searcher;