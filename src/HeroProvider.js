import React, {useState, createContext} from 'react';

export const HeroContext = createContext({});

export const HeroProvider = (props) =>  {
    const [heros, setHeros] = useState([]);
    const [fetching, setFetching] = useState(false);
    return (
        <HeroContext.Provider value={{heros, setHeros, fetching, setFetching}}>
            {props.children}
        </HeroContext.Provider>
    )
}